<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get("/","Controller@home")->name('home');
Route::get("/home","Controller@home")->name('home');
Route::get("/register","Controller@register")->name('register');
Route::post("/register","Controller@register")->name('register');
Route::post("/authenticate","LoginController@authenticate")->name('authenticate');
Route::get("/login","LoginController@login")->name('login');
Route::get("/logout","LoginController@logOut")->name('logOut');


//application
Route::get("/app/view/{id}","ApplicationController@show")->name('app_one');
Route::get("/app/add","ApplicationController@store")->name('app_add')->middleware(['auth']);
Route::post("/app/add","ApplicationController@store")->name('app_add');


Route::post("/deploy-from-git/{id}","ApplicationController@deployFromGit")->name('deploy_from_git');
