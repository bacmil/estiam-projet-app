<?php

namespace App\model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Application extends Model
{
    //
    function source(){
        return $this->hasOne(Source::class);
    }
    function database(){
        return $this->hasOne(Database::class);
    }

    function creator(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    function getApp_url(){

       return env('APP_URL').$this->url;
    }
}
