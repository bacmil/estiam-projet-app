<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    function application(){
        return $this->belongsTo(Application::class);
    }
}
