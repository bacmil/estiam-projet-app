<?php


namespace App;


class Constant
{
    public static $ACCOUNT_TYPE_STUDENT="ACCOUNT_TYPE_STUDENT";
    public static $ACCOUNT_TYPE_TEACHER="ACCOUNT_TYPE_TEACHER";
    public static $ACCOUNT_TYPE_ADMIN="ACCOUNT_TYPE_ADMIN";

    public static $SOURCE_TYPE_ZIP="SOURCE_TYPE_ZIP";
    public static $SOURCE_TYPE_GIT="SOURCE_TYPE_GIT";

    #public static $WEB_FILE_DIR="/home/vagrant/www";
    public static $WEB_FILE_DIR="/var/www/estiam/site";
    public static $WEB_FILE_DIR_ONLINE="/site";
}
