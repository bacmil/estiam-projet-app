<?php

namespace App\Http\Controllers;

use App\Constant;
use App\model\Application;
use App\model\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return  view('application_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        if($request->method()== Request::METHOD_GET){
            return view('app-add');
        }
        $appname = $request->get("name");//get from request
        $appdesc = $request->get("description");//get from request
        $app = new Application();
        $app->name = $appname;
        $app->description = $appdesc;
        $app->url =  "/" .  Str::slug($appname);
        $app->user_id= Auth::user()->id;
        $app->save();

        return redirect()->route("app_one", ["id" => $app->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\model\Application $application
     */
    public function show($id)
    {
        $application = Application::with('creator')->with('source')->where('id',"=",$id)->get()->first();

        return view("app-one", ["app" => $application]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\model\Application $application
     * @return \Illuminate\Http\Response
     */
    public function edit(Application $application)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\model\Application $application
     */
    public function update(Request $request, Application $application)
    {
        $url = $request->get("url");
        $application->url= Constant::$WEB_FILE_DIR."/".$url;

        $application->save();
        return view("application_one", ["app" => $application]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\model\Application $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        //
    }


    public function deployFromGit(Request $request,$id)
    {
        $gitUrl = $request->get('git_url'); // $request->get("git_url"); get from
        $application = Application::where("id","=",$id)->get()->first();
        $appname = $application->name;
        $process = new Process(['git', 'clone', $gitUrl, Constant::$WEB_FILE_DIR . "/" . Str::slug($appname)]);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
             //throw new ProcessFailedException($process);
            echo $process->getErrorOutput();
        } else {
            $source = new Source();
            $source->type = Constant::$SOURCE_TYPE_GIT;
            $source->git_url = $gitUrl;
            $source->application_id = $application->id;
            $source->save();
        }

        return redirect()->route('app_one',[$application->id]);
    }
    public function updateDeployFromGit(Application $application)
    {
        $source = Source::where("application_id","=",$application->id)->get()->first();
        $gitUrl = $source->git_url;  // $request->get("git_url"); get from
        $appname = $application->name;
        $process = new Process(['rm', '-rf', Constant::$WEB_FILE_DIR . "/" .Str::slug($appname)]);
        $process->run();
        // delete old application
        $process = new Process(['git', 'clone', $gitUrl, Constant::$WEB_FILE_DIR . "/" . Str::slug($appname)]);
        $process->run();
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            // throw new ProcessFailedException($process);
            echo $process->getErrorOutput();
        }

        return view("application_one", ["app" => $application]);
    }

    public function updateDeployFromZip(Request $request,Application $application)
    {
        $tempfileurl = ""; //upload temp file url
        $appname = $application->name;//get from app id

        $process = new Process(['rm', '-rf', Constant::$WEB_FILE_DIR . "/" . $appname]);
        $process->run();
        $process = new Process(['tar', '-xfz', $tempfileurl, Constant::$WEB_FILE_DIR . "/" . $appname]);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            // throw new ProcessFailedException($process);
            echo $process->getErrorOutput();
        }
        //delete temp file
        $process = new Process(['rm',  $tempfileurl]);
        $process->run();

        return view("application_one", ["app" => $application]);
    }

    public function deployFromZip(Request $request,Application $application)
    {
        $tempfileurl = ""; //upload temp file url
        $appname = $application->name;//get from app id
        $process = new Process(['tar', '-xfz', $tempfileurl, Constant::$WEB_FILE_DIR . "/" . $appname]);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            // throw new ProcessFailedException($process);
            echo $process->getErrorOutput();
        } else {
            $source = new Source();
            $source->type = Constant::$SOURCE_TYPE_ZIP;
            $source->application_id = $application->id;
            $source->save();
        }
        //delete temp file
        $process = new Process(['rm',  $tempfileurl]);
        $process->run();

        return view("application_one", ["app" => $application]);
    }
}
