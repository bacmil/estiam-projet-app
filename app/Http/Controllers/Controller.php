<?php

namespace App\Http\Controllers;

use App\Constant;
use App\model\Application;
use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function register(Request $request){
        if($request->method()==Request::METHOD_POST) {
            $user = new User();
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->email_verified_at = new \DateTime();
            $user->password = Hash::make( $request->get('password'));
            $user->account_type= Constant::$ACCOUNT_TYPE_STUDENT;
            $user->save();
            return redirect()->route('login');
        }
        return view('register');
    }

    public function home(){
        $applications = Application::all();
        return view('home')->with('applications',$applications);
    }

}
