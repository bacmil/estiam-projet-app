@extends('base')

@section('title') - Accueil @endsection

@section('main')


    <div class="login-box" style="color: white">

        <h2>Les applications</h2>

        <table>
            <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th>auteur</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @if(count($applications)>0)
                @foreach($applications as $app)
                    <tr>
                        <td>{{$app->id}}</td>
                        <td> <a href="{{route('app_one',[$app->id])}}"> {{$app->name}} </a></td>
                        <td>{{$app->creator->name}}</td>
                        <td><a href="{{env('APP_URL').$app->url}}" target="_blank">Visiter</a></td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4">Aucune application !!!</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>


@endsection

