@extends('base')
@section('title')
    - connexion
@endsection

@section('main')

    <div class="login-box">
        <h2>CONNEXION</h2>
        <form action="{{ route('authenticate') }}" method="post">
            @csrf
            <div class="user-box">
                <input type="text" name="email" required="" />
                <label>Email</label>
            </div>
            <div class="user-box">
                <input type="password" name="password" required="" />
                <label>Mot de Passe</label>
            </div>
            <button type="submit" class="form-btn">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                Valider
            </button>
            <a href="{{route('register')}}" style="float: right">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                Nouveau?
            </a>
        </form>
    </div>
@endsection

