<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <link rel="stylesheet" href="{{\Illuminate\Support\Facades\URL::asset('acceuil.css')}}"/>
    <link rel="stylesheet" href="{{\Illuminate\Support\Facades\URL::asset('login.css')}}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Estiam app Deploy @section('title')  @show</title>
    <meta charset="utf-8">
</head>
<img class="center" src="{{\Illuminate\Support\Facades\URL::asset('estiam.png')}}" width="125" height="60" alt="" title=""/>
<nav>
    <ul>
        <li>
            <a href="{{route('home')}}">Acceuil</a>
        </li>
        <li>
            <a href="{{route('app_add')}}">Ajouter Une Application</a>
        </li>
        @auth()
            <li>
                <a href="#">Mes applications</a>
            </li>
        @endauth

        <input type="text" placeholder="Search.."/>
    </ul>

</nav>
<body>
@section('main')
@show
</body>
</html>

