@extends('base')

@section('main')

    <div class="login-box">
        <h2>INSCRIPTION</h2>
        <form action="{{ route('register') }}" method="post">
            @csrf
            <div class="user-box">
                <input type="text" name="name"  required>
                <label>Pseudo</label>
            </div>
            <div class="user-box">
                <input type="text" name="email"required>
                <label>Adresse e-mail</label>
            </div>
            <div class="user-box">
                <input type="password" name="password"  required>
                <label>Mot de Passe</label>
            </div>

            <div>
                <button type="submit" class="form-btn">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Valider
                </button>
                <a href="{{route('login')}}" style="float: right">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Déjà Inscrit?
                </a>
            </div>
        </form>
    </div>
@endsection

