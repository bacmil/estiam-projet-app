@extends('base')

@section('main')

    <div class="login-box" style="color: white">
        <h1>Application {{$app->name}}</h1>

        <p>
            par : {{$app->creator->name}}
        </p>
        <p>
            {{$app->description}}
        </p>

        @if($app->source!=null)
            <h3><a href="{{env('APP_URL').$app->url}}" target="_blank">Visiter</a></h3>
        @endif
        <div>
            @if($app->source==null || $app->source!=null && $app->source->type==\App\Constant::$SOURCE_TYPE_ZIP)
                <h4>Uploader les sources via Git</h4>
                <form method="post" action="{{route('deploy_from_git',[$app->id])}}">
                    @csrf
                    <input type="url" name="git_url" placeholder="Lien vers le depot git">
                    <button type="submit" class="form-btn">Cloner</button>
                </form>
            @endif
            @if($app->source!=null && $app->source->type==\App\Constant::$SOURCE_TYPE_GIT)
                <h4><a href="#"> Mettre à jour via le dépot </a></h4>
            @endif
            @if($app->source==null || $app->source!=null && $app->source->type==\App\Constant::$SOURCE_TYPE_GIT)
                <h4>Uploader ou mettre a jour via un fichier zip</h4>
                <form method="post">
                    @csrf
                    <input type="file" name="zip_file" placeholder="Lien vers le depot git">
                    <button type="submit" class="form-btn">déployer</button>
                </form>
            @endif


        </div>
    </div>


@endsection

