@extends('base')

@section('main')


    <div class="login-box">
        <h2>Ajouter Une Application</h2>
        <form action="{{ route('app_add') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="user-box">
                <input type="text" name="name"/>
                <label>Nom</label>
            </div>
            <div class="user-box">
                <textarea rows="6" placeholder="Description de l'application" name="description" style="width: 100%;"></textarea>
                <label>Description</label>
            </div>
            <br/>
            <div class="user-box">
                <input type="url" name="git_link" >
                <label for="file">Ajouter des sources GIT</label>
            </div>
            <div class="user-box">
                <label for="file">Ajouter des sources ZIP</label></br>
                <input type="file" name="zip_file" placeholder="Fichier de source zip">
            </div>
            <div>
                <button type="submit">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Envoyer
                </button>
            </div>
        </form>
    </div>
@endsection

